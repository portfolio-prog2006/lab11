
use std::{io};
use std::fs::File;
use std::io::{BufRead, BufReader};
use regex::{Regex};

// Reading each line from a bufreader file to a String vector and returning populated vector
fn read_file(f: BufReader<File>) -> Vec<String>{
    let mut v = Vec::new();
    for line in f.lines() {
        v.push(line.unwrap());
    }
    // Returning populated vector
    v
}

// Function that, using regex crate, finds matches for specified filter (regular expression) in the input string,
// returns a vector where each element is the first and last digits found per line concatenated to a two-digit number
fn find_numbers(v:Vec<String>) -> Vec<u32> {
    let mut res: Vec<u32> = Vec::new();

    // Iterating over each line
    for l in v {
        // Specifying which input string to filter on using regex
        let input = l.as_str();
        // Specifying the expression to use when finding matches
        let re = Regex::new(r"[1-9]|one|two|three|four|five|six|seven|eight|nine").unwrap();

        // Initializing first and last variables
        let mut first_number: Option<u32> = None;
        let mut last_number: Option<u32> = None;

        // Parsing the captured values to their corresponding numerical digit (if they're number words)
        for cap in re.captures_iter(input) {
            let num_str = cap.get(0).unwrap().as_str();
            let num = match num_str {
                "one" => 1,
                "two" => 2,
                "three" => 3,
                "four" => 4,
                "five" => 5,
                "six" => 6,
                "seven" => 7,
                "eight" => 8,
                "nine" => 9,
                _ => num_str.parse().unwrap(),
            };

            // Setting first and last numbers
            if first_number.is_none() {
                first_number = Some(num);
            }
            last_number = Some(num);
        }

        // Concatenating first and last numbers into a two digit one, by multiplying 'first' by 10 then adding 'last'
        if let (Some(first), Some(last)) = (first_number, last_number) {
            let concatenated = first * 10 + last;
            res.push(concatenated);
        }
    }
        res
}
// Finds the bearing (number mod 360)
fn find_bearing(v: Vec<u32>) -> u32 {

    // finding the sum by iterating over each element and calling sum() on it
    let sum: u32 = v.iter().sum();
    // creating the bearing, which is 'sum mod 360'
    let bearing = sum % 360;

    // returning
    bearing
}

// Main entry point
fn main() -> io::Result<()>{
    // Opening the file, turning it into a bufreader file
    let f = File::open("./src/data2.txt")?;
    let f = BufReader::new(f);

    // Storing each line as an element in vector v3
    let v = read_file(f);

    // Saving numbers found in the file to a vector, each line a two-digit number (as string)
    let v2 = find_numbers(v);
    // Creating and adding correct values (from data2.txt) to our test vector
    let v3: Vec<u32> = vec![29, 84, 13, 24, 42, 14, 76];

    // Printing results (to see that they're equal and the program works)
    println!("{:?}", v2);
    println!("{:?}", v3);

    // Finding the bearing (sum of all numbers in v2 mod 360) in both file vector and test vector
    let result = find_bearing(v2);
    let result2 = find_bearing(v3);

    // Printing the result
    println!("\nThe final bearing is {}", result);
    println!("The final test bearing is {}", result2);


    Ok(())
}
