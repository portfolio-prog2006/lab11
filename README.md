# Lab11 - Bearing in Space (Rust)

Using standard input, file manipulation, modular code. 

## Learning objectives for Lab11, as outlined by lecturer

> ### Objectives for Lab11
> The code should be:
> - elegant
> - composable (modular)
> - easy to maintain, update, and modify

----------------
### Prelim. info

*Crates used for this program:*

- regex = "1.10.4"


## The task - Find the Bearing

A program that reads from a file/standard input and analyzes (adhering to requirements) the contents, to return the final bearing for a space voyage.

The task includes:
- A function that reads sequences of numbers from std in or file. Each line is a single alien inscription
- Extract two-digit numbers from each line. These are the first and last digits in the line. Number words (e.g., "one", "two",...) should be treated as numbers as well
- Digits as english words - Numerical words (e.g., "one", "two",...) should be treated as valid digits as well

#### Brief on the modular functions within the program:
- `read_file(..)` - Reads each line from a bufreader file to a String vector and returning a populated vector
- `find_numbers(..)` - Function that, using regex crate, finds matches for a specified filter (regular expression) in the input string. Returns first and last digit per line as a vec
- `find_bearing(..)` - Takes a vec as param, finds the bearing (sum of vec mod 360) and returns it

--------------

Using these we get:

### *For input...*
```
two3nine
eightwofour
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen

```

The calculated numbers would be `29, 84, 13, 24, 42, 14, 76`. Adding these together produces `282`. The final bearing is this number mod 360. So,...

### *...output would be *

```
282 
```
